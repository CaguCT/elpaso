var util = require('gulp-util');

var destPath = 'build';
var srcPath = 'src';
var production = util.env.production || util.env.prod || false;

var config = {
  env :        'development',
  production : production,
  serverPort : 8080,

  node_modules : 'node_modules',
  src :  {
    root :          srcPath,
    static :        'static',
    templates :     srcPath + '/templates',
    templatesData : srcPath + '/templates/data',
    pageList :      srcPath + '/index.yaml',
    sass :          srcPath + '/sass',
    // path for sass files that will be generated automatically via some of tasks
    sassGen :       srcPath + '/sass/generated',
    js :            srcPath + '/js',
    img :           srcPath + '/img',
    svg :           srcPath + '/img/svg',
    // path to png sources for sprite:png task
    iconsPng :      srcPath + '/icons',
    // path to svg sources for sprite:svg task
    iconsSvg :      srcPath + '/icons',
    icons :         srcPath + '/icons',
    iconsFont :     srcPath + '/icons',
    fonts :         srcPath + '/fonts',
    lib :           srcPath + '/lib'
  },
  dest : {
    root :   destPath,
    html :   destPath,
    css :    destPath + '/css',
    js :     destPath + '/js',
    img :    destPath + '/img',
    fonts :  destPath + '/fonts',
    node_libs_js :    destPath + '/node_libs',
    node_libs_css :    destPath + '/node_css',
    lib :    destPath + '/lib',
    icons :    destPath + '/icons',
    design : destPath + '/design'
  },

  setEnv : function(env) {
    if (typeof env !== 'string') return;
    this.env = env;
    this.production = env === 'production';
    process.env.NODE_ENV = env;
  },

  logEnv :       function() {
    util.log(
      'Environment:',
      util.colors.white.bgRed(' ' + process.env.NODE_ENV + ' ')
    );
  },
  errorHandler : require('./util/handle-errors')
};

module.exports = config;
