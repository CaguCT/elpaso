var app = {
  init :                 function() {
    console.log('Hi there 😎');

    app.clickEvents();
    app.changeEvents();
    app.autocomplete();
    app.select2();
    app.datePicker();
    app.dateRangePicker();
    app.chartJs();
    app.inputDefault();

    app.progressRing();

    popup.init();

    // todo: remove
    app.testData();

  },
  setProgress :          function(percent) {
    var circle = document.getElementById('progress-ring__circle');
    var radius = circle.r.baseVal.value;
    var circumference = radius * 2 * Math.PI;
    var sDIffset = +circumference - percent / 100 * circumference;
    circle.style.strokeDashoffset = sDIffset;
  },
  progressRing :         function() {
    var circle = document.getElementById('progress-ring__circle');
    var radius = circle.r.baseVal.value;
    var circumference = radius * 2 * Math.PI;

    circle.style.strokeDasharray = circumference + ' ' + circumference;
    circle.style.strokeDashoffset = circumference;

    app.setProgress(25);
  },
  progressRingStart :    function() {
    var start = +$('.successfull__value').text();
    var now = start;
    var intervalId = null;
    if (start > 0) {
      app.setProgress(0);
      intervalId = setInterval(function() {
        now = now - 1;

        var percent_now = now * 100 / start;
        var progress_circle = 100 - percent_now;
        app.setProgress(progress_circle);
        $('.successfull__value').text(now);

        if (now < 1) {
          clearInterval(intervalId);
        }
      }, 1000);
    }
  },
  inputDefault :         function() {
    var $input = $('input[data-default]');
    if ($input.length) {
      $input.on('focus', function() {
        var $self = $(this);
        var initialVal = $self.val();

        if ($self.hasClass('not-default') === false) {
          $self.val(initialVal);
        }
      });
      $input.on('input', function() {
        var $self = $(this);
        var defaultText = typeof $self.attr('data-default') !== 'undefined' ? $self.attr('data-default') : '';

        if ($self.val() !== defaultText) {
          $self.addClass('not-default');
        } else {
          $self.removeClass('not-default');
        }
      });
    }
  },
  testData :             function() {
    var $taskCheck = $('#taskCheck-1');
    if ($taskCheck.length) {
      $taskCheck.prop('checked', true);
    }

  },
  dateRangePicker :      function() {
    // var start = moment('2018-06-21', 'YYYY-MM-DD');
    // var end = moment('2018-07-20', 'YYYY-MM-DD');
    var first = true;

    function cb(start, end) {
      jQuery('[data-target="daterangepicker"] span')
        .html(start.format('MMMM D, YYYY') + ' - ' +
        end.format('MMMM D, YYYY'));

      jQuery('.date_send input[name="date_start"]').val(start.format('YYYY-MM-DD'));
      jQuery('.date_send input[name="date_end"]').val(end.format('YYYY-MM-DD'));

      if (first === false) {
        console.log('Send data!');
      }

      if (first)
        first = false;
    }

    jQuery('[data-target="daterangepicker"]').daterangepicker({
      ranges : {
        'Today' :        [moment(), moment()],
        'Yesterday' :    [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 days' :  [moment().subtract(6, 'days'), moment()],
        'Last 30 days' : [moment().subtract(29, 'days'), moment()],
        'This month' :   [moment().startOf('month'), moment().endOf('month')],
        'Last month' :   [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
    }, cb);
  },
  select2 :              function() {
    var $select2 = $('.select2');

    if ($select2.length) {
      $select2.select2({
        minimumResultsForSearch : Infinity
      });
    }
  },
  chartJsRandomData :    function(val) {
    var d = new Date();
    var now = d.getDate();
    now = 24;
    var labels = [];
    var data = [];
    var day = 0;
    val = typeof val !== 'undefined' ? +val : 6;

    for(var i = 0; i < 31; i++) {
      day = +i + 1;

      if (day <= now) {
        data[i] = +Math.random().toFixed(2) + val;
      }

      if (day < 10) {
        day = '0' + day;
      }

      labels[i] = day;
    }

    return {data : data, labels : labels};
  },
  chartJs :              function() {
    var $ctx = $('.chart-js');
    var myCharts = {};

    $ctx.each(function(i, ctx) {
      randomData = app.chartJsRandomData(6 * (i + 1));
      myCharts[i] = new Chart(ctx, {
        type :    'line',
        label :   '1',
        data :    {
          labels :   randomData.labels,
          datasets : [{
            label :                '# of Votes',
            data :                 randomData.data,
            borderColor :          'rgba(28, 154, 247,1)',
            backgroundColor :      'transparent',
            borderWidth :          3,
            pointRadius :          6,
            pointBackgroundColor : 'rgba(28, 154, 247,1)',
            pointBorderColor :     'rgba(255, 255, 255,1)'
          }]
        },
        options : {
          scales : {
            yAxes : [{
              ticks : {
                beginAtZero : false
              }
            }]
          }
        }
      });
    });

  },
  datePicker :           function() {
    var $datePicker = $('.datepicker');
    if ($datePicker.length) {
      $datePicker.find('.datepicker-input').datepicker({});
    }
  },
  autocomplete :         function() {
    var $autoCompletes = $('.autocomplete');
    if ($autoCompletes.length && typeof $('<div/>').autocomplete !== 'undefined') {

      $autoCompletes.each(function(i, item) {
        var $autoComplete = $(item);
        var url = $autoComplete.attr('data-url');

        $autoComplete.autocomplete({
          source :    function(request, response) {
            $.ajax({
              url :           url,
              dataType :      'jsonp',
              jsonpCallback : 'autocomplete',
              data :          {
                term : request.term
              },
              success :       function(data) {
                response(data);
              }
            });
          },
          create :    function() {
            $(this).attr('autocomplete', 'nope');
          },
          minLength : 0,
          open :      function() {
            var $ul = $('.ui-autocomplete');
            var counterClass = 'ui-autocomplete-counter';
            var len = $ul.find('li:not(".' + counterClass + '")').length;
            var $counter = $ul.find('.' + counterClass);

            if ($counter.length) {
              $counter.text(len + ' Results found');
            } else {
              $counter = $('<li>').addClass(counterClass);
              $ul.prepend($counter);
            }

            $counter.text(len + ' Results found');

            $ul.prepend($('<li>').addClass(counterClass).text(len + ' Results found'));
          },
          select :    function() {
            console.log($(this).val());
          }
        }).autocomplete('instance')._renderItem = function(ul, item) {
          var $template = $('<div>' +
            '<div class="ui-menu-item-image">' +
            '<img />' +
            '</div>' +
            '<div class="ui-menu-item-content">' +
            '<div class="ui-menu-item-name"></div>' +
            '<div class="ui-menu-item-alias"></div>' +
            '</div>' +
            '</div>');

          if (typeof item.image === 'undefined') {
            $template.find('.ui-menu-item-image').remove();
          } else {
            $template.find('.ui-menu-item-image img').attr('src', item.image);
          }
          if (typeof item.alias === 'undefined') {
            $template.find('.ui-menu-item-alias').remove();
          } else {
            $template.find('.ui-menu-item-alias').text(item.alias);
          }

          $template.find('.ui-menu-item-name').text(item.value);

          return $('<li>')
            .append($template)
            .appendTo(ul);
        };
      });

    }
  },
  changeEvents :         function() {
    var $d = $(document.body);
    var events = 'change';

    $d.on(events, '.custom-checkbox [type="checkbox"][data-target]', function(e) {
      e.preventDefault();

      app.customCheckboxCheck($(this));
    });

    $d.on(events, '.custom-checkbox [type="checkbox"][data-toggle]', function(e) {
      e.preventDefault();

      app.customCheckboxToggle($(this));
    });

  },
  clickEvents :          function() {
    var $d = $(document.body);
    var events = 'click';

    $d.on(events, '.profile', function(e) {
      e.preventDefault();

      app.profileToggle($(this));
    });

    $d.on(events, '.custom-control-select, .custom-control-deselect', function(e) {
      e.preventDefault();

      app.selectDeselectAll($(this));
    });

    $d.on(events, '.transaction-item__header', function(e) {
      e.preventDefault();

      var $item = $(this).closest('.transaction-item');
      app.transactionToggle($item);
    });

    $d.on(events, '.contact-item__toggle', function(e) {
      e.preventDefault();

      var $item = $(this).closest('.contact-item');
      $item.toggleClass('open');
    });

    $d.on(events, '.js-dropdown-menu', function(e) {
      e.stopPropagation();
    });

    $d.on(events, '.shareholder__add', function(e) {
      e.preventDefault();

      app.shareholderAdd($(this));
    });

    $d.on(events, '.shareholder__remove', function(e) {
      e.preventDefault();

      app.shareholderRemove($(this));
    });

    $d.on(events, '.ring-demo', function(e) {
      e.preventDefault();

      app.progressRingStart();
    });

  },
  shareholderRemove :    function($button) {
    var $tr = $button.closest('[data-id]');
    $tr.remove();
  },
  shareholderAdd :       function() {
    var $table = $('.shareholder__table');
    var lastId = +$table.find('tr').last().attr('data-id');
    var nextId = lastId > 0 ? lastId + 1 : 1;
    var template = '<tr data-id="' + nextId + '">\n' +
      '    <td>\n' +
      '        <a href="#" class="shareholder__edit">Shareholder ' + nextId + '</a>\n' +
      '        <div class="shareholder__type">Type</div>\n' +
      '    </td>\n' +
      '    <td></td>\n' +
      '    <td>Beneficial<br>User</td>\n' +
      '    <td>\n' +
      '        <a href="%23" class="shareholder__remove"><svg class="icon icon-trash">\n' +
      '                              <use xlink:href="img/sprite.svg#icon-trash"></use>\n' +
      '                            </svg></a>\n' +
      '    </td>\n' +
      '</tr>';

    var $template = $(template);

    $table.append($template);

  },
  transactionToggle :    function($self) {
    $self.toggleClass('open');
  },
  customCheckboxToggle : function($self) {
    var $target = $($self.attr('data-toggle'));
    if ($target.length) {
      if ($self.prop('checked')) {
        $target.addClass('toggle');
      } else {
        $target.removeClass('toggle');
      }
    }
  },
  customCheckboxCheck :  function($self) {
    var $customCheckbox = $self.closest('.custom-checkbox');
    var $customControlSelect = $customCheckbox.find('.custom-control-select');
    var $customControlDeselect = $customCheckbox.find('.custom-control-deselect');

    if ($(this).prop('checked')) {
      app.selectDeselectAll($customControlSelect);
    } else {
      app.selectDeselectAll($customControlDeselect);
    }
  },
  selectDeselectAll :    function($self) {
    var $customCheckbox = $self.closest('.custom-checkbox');
    var $checkbox = $customCheckbox.find('[type="checkbox"]');
    var target = $self.attr('data-target');
    var selectClass = 'custom-control-select';
    var isSelect = $self.hasClass(selectClass);

    if (typeof target !== 'undefined') {
      var $target = $(target);
      if ($target.length) {
        $target.prop('checked', isSelect);
        $checkbox.prop('checked', isSelect);
      }
    }
  },
  profileToggle :        function($self) {
    var openClass = 'open';
    $self.toggleClass(openClass);
  }
};

var popup = {
  selector :    '.popup',
  init :        function() {
    popup.clickEvents();

    if (location.hash === '#transaction') {
      popup.open($('#transaction'));
    }
  },
  clickEvents : function() {
    var $d = $(document.body);
    var events = 'click';

    $d.on(events, '[data-popup]', function(e) {
      e.preventDefault();

      var popupSelector = $(this).attr('data-popup');
      var $popup = typeof popupSelector !== 'undefined' ? $(popupSelector) : null;

      if ($popup !== null && $popup.length) {
        popup.open($popup);
      }
    });

    $d.on(events, '.popup-close', function(e) {
      e.preventDefault();

      var $popup = $(this).closest(popup.selector);
      popup.close($popup);
    });

    $d.on(events, popup.selector, function(e) {

      if (e.target === this) {
        e.preventDefault();
        var $popup = $(e.target);
        popup.close($popup);
      }
    });
  },
  open :        function($popup) {
    $(document.body).addClass('popup-open');
    $popup.removeClass('hide');
  },
  close :       function($popupItem) {
    $(document.body).removeClass('popup-open');
    $popupItem.addClass('hide');
  }
};

$(function() {
  app.init();
});
